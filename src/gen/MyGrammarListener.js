// Generated from MyGrammar.g4 by ANTLR 4.9
// jshint ignore: start
import antlr4 from 'antlr4';

// This class defines a complete listener for a parse tree produced by MyGrammarParser.
export default class MyGrammarListener extends antlr4.tree.ParseTreeListener {

	// Enter a parse tree produced by MyGrammarParser#program.
	enterProgram(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#program.
	exitProgram(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#vardeclr.
	enterVardeclr(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#vardeclr.
	exitVardeclr(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#field_declr.
	enterField_declr(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#field_declr.
	exitField_declr(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#array_id.
	enterArray_id(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#array_id.
	exitArray_id(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#field_var.
	enterField_var(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#field_var.
	exitField_var(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#var_id.
	enterVar_id(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#var_id.
	exitVar_id(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#method_declr.
	enterMethod_declr(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#method_declr.
	exitMethod_declr(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#return_type.
	enterReturn_type(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#return_type.
	exitReturn_type(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#block.
	enterBlock(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#block.
	exitBlock(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#statement.
	enterStatement(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#statement.
	exitStatement(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#method_call_inter.
	enterMethod_call_inter(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#method_call_inter.
	exitMethod_call_inter(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#method_call.
	enterMethod_call(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#method_call.
	exitMethod_call(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#expr.
	enterExpr(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#expr.
	exitExpr(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#location.
	enterLocation(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#location.
	exitLocation(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#callout_arg.
	enterCallout_arg(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#callout_arg.
	exitCallout_arg(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#int_literal.
	enterInt_literal(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#int_literal.
	exitInt_literal(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#rel_op.
	enterRel_op(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#rel_op.
	exitRel_op(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#eq_op.
	enterEq_op(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#eq_op.
	exitEq_op(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#cond_op.
	enterCond_op(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#cond_op.
	exitCond_op(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#literal.
	enterLiteral(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#literal.
	exitLiteral(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#bin_op.
	enterBin_op(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#bin_op.
	exitBin_op(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#arith_op.
	enterArith_op(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#arith_op.
	exitArith_op(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#var_type.
	enterVar_type(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#var_type.
	exitVar_type(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#assign_op.
	enterAssign_op(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#assign_op.
	exitAssign_op(ctx) {
	}


	// Enter a parse tree produced by MyGrammarParser#method_name.
	enterMethod_name(ctx) {
	}

	// Exit a parse tree produced by MyGrammarParser#method_name.
	exitMethod_name(ctx) {
	}



}